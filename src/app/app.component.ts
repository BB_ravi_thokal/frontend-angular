import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

export const LoaderState$ = new Subject<any>();
export const PopUpState$ = new Subject<any>();

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isError = false;
  isSuccess = false;
  message;
  loading = false;
  constructor(
  ) {}

  async ngOnInit() {
    PopUpState$
      .subscribe((data) => {
        if (data.state.toLowerCase() === 'success') {
          this.isSuccess = true;
          this.isError = false;
          this.message = data.message;
        } else if (data.state.toLowerCase() === 'error') {
          this.isError = true;
          this.isSuccess = false;
          this.message = data.message;
        } else {
          this.isError = false;
          this.isSuccess = false;
          this.message = '';
        }
      });

    LoaderState$
    .subscribe((data) => {
      if (data) {
        this.loading = true;
      } else {
        this.loading = false;
      }
    });
  }

  disablePopUp() {
    this.isError = false;
    this.isSuccess = false;
  }
}
