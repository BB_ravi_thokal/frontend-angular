import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { LoginService } from '../login.service';
import * as WebURL from '../../../config/web-urls';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['../../../app.component.scss', '../login.component.scss', './signup.component.scss']
})
export class SignupComponent implements OnInit {
  selectedWareHouse = 'Choose Warehouse';
  userForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.formValidation();
  }

  get f() { return this.userForm.controls; }

  /**
   * This method is for form validation
   */
  formValidation() {
    this.userForm = this.formBuilder.group({
      name: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      contactNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      password: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      emailId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
        ])
      ),
    });
  }

  /**
   * This method is to register user
   */
  async add() {
    LoaderState$.next(true);
    if (this.userForm.valid) {
      this.submitted = true;
      const payload = {
        name: this.userForm.get('name').value,
        password: this.userForm.get('password').value,
        contactNumber: this.userForm.get('contactNumber').value.toString(),
        emailId: this.userForm.get('emailId').value
      };
      await this.loginService.registerUser$(WebURL.CREATE_USER, payload).subscribe({
        next: (results) => {
          LoaderState$.next(false);
          this.router.navigate(['login/']);
        }, error: error => {
            LoaderState$.next(false);
            PopUpState$.next({
              state: 'error',
              message: error.error.message ? error.error.message : 'Not able to register you at the moment. Please try later!'
            });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }

}

