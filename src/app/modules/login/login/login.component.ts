import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import * as WebURL from '../../../config/web-urls';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../../app.component.scss', '../login.component.scss', './login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.formValidation();
  }
  get f() { return this.loginForm.controls; }

  /**
   * This method is for form validation
   */
  formValidation() {
    this.loginForm = this.formBuilder.group({
      userId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
        ])
      ),
      password: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      )
    });
  }

  /**
   * This method is to handle login
   */
  async login() {
    this.submitted = true;
    LoaderState$.next(true);
    if (this.loginForm.valid) {
      const data = {
        userId: this.loginForm.get('userId').value,
        password: this.loginForm.get('password').value
      };
      await this.loginService.login$(WebURL.LOGIN, data).subscribe({
        next: (results) => {
          LoaderState$.next(false);
          localStorage.setItem('access_token', results.data.token);
          localStorage.setItem('userId', results.data.userData._id);
          this.router.navigate(['home/']);
        }, error: error => {
            LoaderState$.next(false);
            PopUpState$.next({
              state: 'error',
              message: error.error.message ? error.error.message : 'Not able to login at the moment. Please try again later'
            });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }

}
