import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from '../../config/web-urls';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LandingService {
    constructor(
        private http: HttpClient,
    ) { }
    /**
     * This method is for Get Api calls
     * @param url String Api URL
     * @param params Object required for the Get request
     * @returns Return the respective response from the Apis.
     */
    public getUser$(url, params): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('userId', params.userId);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    /**
     * This method is for Put Api calls
     * @param url String Api URL
     * @param requestBody Object required for the Post request
     * @returns Return the respective response from the Apis.
     */
    public updateUser$(url, requestBody): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        const httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        return this.http.put<any>(BASE_URL + url, requestBody, httpOptions);
    }

}
