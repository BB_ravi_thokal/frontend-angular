import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../app.component';
import { User } from '../../interface/user.interface';
import { LandingService } from './landing.service';
import * as WebURL from '../../config/web-urls';
import { Router } from '@angular/router';
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  selectedWareHouse = 'Choose Warehouse';
  userForm: FormGroup;
  submitted = false;
  userData: any;
  userId: string;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private landingService: LandingService
  ) { }

  ngOnInit() {
    this.getUserData();
    this.formValidation();
  }

  get f() { return this.userForm.controls; }

  /**
   * This method is to fetch latest user details and display
   */
  async getUserData() {
    this.userId = await localStorage.getItem('userId');
    await this.landingService.getUser$(WebURL.GET_USER, { userId: this.userId }).subscribe({
      next: (results) => {
        LoaderState$.next(false);
        this.userData = results.data.data;
        this.userForm.controls.name.setValue(this.userData.name);
        this.userForm.controls.contactNumber.setValue(this.userData.contactNumber);
        this.userForm.controls.emailId.setValue(this.userData.emailId);
      }, error: error => {
          LoaderState$.next(false);
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'Not able to fetch user details. Please try later!'
          });
      }
    });
  }

  /**
   * This method is for form validation
   */
  formValidation() {
    this.userForm = this.formBuilder.group({
      name: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      contactNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      emailId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
        ])
      ),
    });
  }

  /**
   * This method is to update user details
   */
  async update() {
    LoaderState$.next(true);
    this.submitted = true;
    if (this.userForm.valid) {
      const payload = {
        _id: this.userId,
        name: this.userForm.get('name').value,
        contactNumber: this.userForm.get('contactNumber').value,
        emailId: this.userForm.get('emailId').value
      };
      await this.landingService.updateUser$(WebURL.UPDATE_USER, payload).subscribe({
        next: (results) => {
          PopUpState$.next({
            state: 'success',
            message:  results.message ? results.message :  'Update success!'
          });
          LoaderState$.next(false);
          this.getUserData();
        }, error: error => {
            LoaderState$.next(false);
            PopUpState$.next({
              state: 'error',
              message: error.error.message ? error.error.message : 'Not able to fetch user details. Please try later!'
            });
        }
      });
      LoaderState$.next(false);
    } else {
      LoaderState$.next(false);
      return;
    }
  }

  async logout() {
    await localStorage.clear();
    this.router.navigate(['login/']);
  }

}
