import { Injectable, Inject } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {


  constructor(
    private router: Router,
  ) {
  }

  /**
   * @description This method will check user already login or not
   * @returns Return the true if user already logged in or else false
   */
  public canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    try {
      const isTokenAvailable = localStorage.getItem('access_token');
      if (isTokenAvailable) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    } catch (err) {
      return false;
    }
  }
}
