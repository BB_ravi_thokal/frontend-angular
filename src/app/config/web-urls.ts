export const BASE_URL = 'http://localhost:4000/';

// login urls
export const LOGIN = 'login';
export const FORGOT_PASSWORD = 'login/forgot-password';
export const CHANGE_PASSWORD = 'login/change-password';

// Warehouse urls
export const CREATE_USER = 'user/create';
export const GET_USER = 'user';
export const UPDATE_USER = 'user/update';


