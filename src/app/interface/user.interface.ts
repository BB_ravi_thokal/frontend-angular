export interface User {
    name: string;
    contactNumber: number;
    emailId: string;
}
