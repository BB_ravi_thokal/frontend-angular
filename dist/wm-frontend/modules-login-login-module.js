(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-login-login-module"],{

/***/ "./src/app/modules/login/login-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/modules/login/login-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: LoginRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginRoutingModule", function() { return LoginRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/modules/login/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/modules/login/signup/signup.component.ts");





var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'login',
            },
            { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
            { path: 'signup', component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__["SignupComponent"] },
        ]
    }
];
var LoginRoutingModule = /** @class */ (function () {
    function LoginRoutingModule() {
    }
    LoginRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], LoginRoutingModule);
    return LoginRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/login/login.component.scss":
/*!****************************************************!*\
  !*** ./src/app/modules/login/login.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card0 {\n  box-shadow: 0px 4px 8px 0px #757575;\n  border-radius: 0px; }\n\n.card2 {\n  margin: 0px 40px; }\n\n.logo {\n  width: 200px;\n  height: 100px;\n  margin-top: 20px;\n  margin-left: 35px; }\n\n.image {\n  width: 360px;\n  height: 280px; }\n\n.border-line {\n  border-right: 1px solid #EEEEEE; }\n\n.facebook {\n  background-color: #3b5998;\n  color: #fff;\n  font-size: 18px;\n  padding-top: 5px;\n  border-radius: 50%;\n  width: 35px;\n  height: 35px;\n  cursor: pointer; }\n\n.twitter {\n  background-color: #1DA1F2;\n  color: #fff;\n  font-size: 18px;\n  padding-top: 5px;\n  border-radius: 50%;\n  width: 35px;\n  height: 35px;\n  cursor: pointer; }\n\n.linkedin {\n  background-color: #2867B2;\n  color: #fff;\n  font-size: 18px;\n  padding-top: 5px;\n  border-radius: 50%;\n  width: 35px;\n  height: 35px;\n  cursor: pointer; }\n\n.line {\n  height: 1px;\n  width: 45%;\n  background-color: #E0E0E0;\n  margin-top: 10px; }\n\n.or {\n  width: 10%;\n  font-weight: bold; }\n\n@media screen and (max-width: 991px) {\n  .logo {\n    margin-left: 0px; }\n  .image {\n    width: 300px;\n    height: 220px; }\n  .border-line {\n    border-right: none; }\n  .card2 {\n    border-top: 1px solid #EEEEEE !important;\n    margin: 0px 15px; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBUUE7RUFDSSxtQ0FBbUM7RUFDbkMsa0JBQ0osRUFBQTs7QUFFQTtFQUNJLGdCQUNKLEVBQUE7O0FBRUE7RUFDSSxZQUFZO0VBQ1osYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixpQkFDSixFQUFBOztBQUVBO0VBQ0ksWUFBWTtFQUNaLGFBQ0osRUFBQTs7QUFFQTtFQUNJLCtCQUNKLEVBQUE7O0FBRUE7RUFDSSx5QkFBeUI7RUFDekIsV0FBVztFQUNYLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFDSixFQUFBOztBQUVBO0VBQ0kseUJBQXlCO0VBQ3pCLFdBQVc7RUFDWCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQ0osRUFBQTs7QUFFQTtFQUNJLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixlQUNKLEVBQUE7O0FBRUE7RUFDSSxXQUFXO0VBQ1gsVUFBVTtFQUNWLHlCQUF5QjtFQUN6QixnQkFDSixFQUFBOztBQUVBO0VBQ0ksVUFBVTtFQUNWLGlCQUNKLEVBQUE7O0FBR0E7RUFDSTtJQUNJLGdCQUNKLEVBQUE7RUFFQTtJQUNJLFlBQVk7SUFDWixhQUNKLEVBQUE7RUFFQTtJQUNJLGtCQUNKLEVBQUE7RUFFQTtJQUNJLHdDQUF3QztJQUN4QyxnQkFDSixFQUFBLEVBQUMiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gYm9keSB7XG4vLyAgICAgY29sb3I6ICMwMDA7XG4vLyAgICAgb3ZlcmZsb3cteDogaGlkZGVuO1xuLy8gICAgIGhlaWdodDogMTAwJTtcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjQjBCRUM1O1xuLy8gICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXRcbi8vIH1cblxuLmNhcmQwIHtcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDhweCAwcHggIzc1NzU3NTtcbiAgICBib3JkZXItcmFkaXVzOiAwcHhcbn1cblxuLmNhcmQyIHtcbiAgICBtYXJnaW46IDBweCA0MHB4XG59XG5cbi5sb2dvIHtcbiAgICB3aWR0aDogMjAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAzNXB4XG59XG5cbi5pbWFnZSB7XG4gICAgd2lkdGg6IDM2MHB4O1xuICAgIGhlaWdodDogMjgwcHhcbn1cblxuLmJvcmRlci1saW5lIHtcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjRUVFRUVFXG59XG5cbi5mYWNlYm9vayB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNiNTk5ODtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgd2lkdGg6IDM1cHg7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIGN1cnNvcjogcG9pbnRlclxufVxuXG4udHdpdHRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzFEQTFGMjtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgd2lkdGg6IDM1cHg7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIGN1cnNvcjogcG9pbnRlclxufVxuXG4ubGlua2VkaW4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMyODY3QjI7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIHdpZHRoOiAzNXB4O1xuICAgIGhlaWdodDogMzVweDtcbiAgICBjdXJzb3I6IHBvaW50ZXJcbn1cblxuLmxpbmUge1xuICAgIGhlaWdodDogMXB4O1xuICAgIHdpZHRoOiA0NSU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0UwRTBFMDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4XG59XG5cbi5vciB7XG4gICAgd2lkdGg6IDEwJTtcbiAgICBmb250LXdlaWdodDogYm9sZFxufVxuXG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gICAgLmxvZ28ge1xuICAgICAgICBtYXJnaW4tbGVmdDogMHB4XG4gICAgfVxuXG4gICAgLmltYWdlIHtcbiAgICAgICAgd2lkdGg6IDMwMHB4O1xuICAgICAgICBoZWlnaHQ6IDIyMHB4XG4gICAgfVxuXG4gICAgLmJvcmRlci1saW5lIHtcbiAgICAgICAgYm9yZGVyLXJpZ2h0OiBub25lXG4gICAgfVxuXG4gICAgLmNhcmQyIHtcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNFRUVFRUUgIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luOiAwcHggMTVweFxuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/modules/login/login.module.ts":
/*!***********************************************!*\
  !*** ./src/app/modules/login/login.module.ts ***!
  \***********************************************/
/*! exports provided: LoginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModule", function() { return LoginModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/modules/login/login-routing.module.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/modules/login/login/login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/modules/login/signup/signup.component.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login.service */ "./src/app/modules/login/login.service.ts");








var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"],
                _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__["SignupComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _login_routing_module__WEBPACK_IMPORTED_MODULE_3__["LoginRoutingModule"]
            ],
            providers: [_login_service__WEBPACK_IMPORTED_MODULE_7__["LoginService"]],
        })
    ], LoginModule);
    return LoginModule;
}());



/***/ }),

/***/ "./src/app/modules/login/login.service.ts":
/*!************************************************!*\
  !*** ./src/app/modules/login/login.service.ts ***!
  \************************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../config/web-urls */ "./src/app/config/web-urls.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var LoginService = /** @class */ (function () {
    function LoginService(http) {
        this.http = http;
    }
    /**
     * This method is for Post Api call
     * @param url String Api end point
     * @param requestBody Object required for the Post request
     * @returns Return the respective response from the Apis.
     */
    LoginService.prototype.login$ = function (url, requestBody) {
        console.log(url, 'url');
        console.log(_config_web_urls__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"], 'BASE_URL');
        console.log(requestBody, 'requestBody');
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.post(_config_web_urls__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"] + url, requestBody, httpOptions);
    };
    /**
     * This method is for Post Api calls
     * @param url String Api URL
     * @param requestBody Object required for the Post request
     * @returns Return the respective response from the Apis.
     */
    LoginService.prototype.registerUser$ = function (url, requestBody) {
        console.log(url, 'url');
        console.log(_config_web_urls__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"], 'BASE_URL');
        console.log(requestBody, 'requestBody');
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.post(_config_web_urls__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"] + url, requestBody, httpOptions);
    };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/modules/login/login/login.component.html":
/*!**********************************************************!*\
  !*** ./src/app/modules/login/login/login.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto\">\n    <div class=\"card card0 border-0\">\n        <div class=\"row d-flex\">\n            <div class=\"col-lg-12\">\n                <div class=\"card2 card border-0 px-4 py-5\">\n                    <form class=\"form-signin\" [formGroup]=\"loginForm\">\n                        <div class=\"row mb-4 px-2 py-2\">\n                            <h2>Login to access your account</h2>\n                        </div>\n                        <div class=\"form-group row px-3\">\n                            <label class=\"mb-1\">\n                                <h6 class=\"mb-0 text-sm\">Email Id</h6>\n                            </label>\n                            <input formControlName=\"userId\" [ngClass]=\"{ 'is-invalid': submitted && f.userId.errors }\"\n                                type=\"text\" class=\"form-control\" id=\"email\" placeholder=\"Enter a valid email address\">\n                            <div *ngIf=\"submitted && f.userId.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.userId.errors.required\">User id is required</div>\n                                <div *ngIf=\"f.userId.errors.pattern\">Please enter valid email id</div>\n                            </div>\n\n                        </div>\n                        <div class=\"form-group row px-3\">\n                            <label class=\"mb-1\">\n                                <h6 class=\"mb-0 text-sm\">Password</h6>\n                            </label>\n                            <input formControlName=\"password\"\n                                [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" class=\"input form-control\"\n                                type=\"password\" name=\"password\" placeholder=\"Enter password\">\n                            <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.password.errors.required\">Password is required</div>\n                            </div>\n                        </div>\n                        <div class=\"row px-3 mb-4\">\n                            <div class=\"custom-control custom-checkbox custom-control-inline\"> <input id=\"chk1\"\n                                    type=\"checkbox\" name=\"chk\" class=\"custom-control-input\"> </div>\n                            <a [routerLink]=\"['/login/forgot-password']\" class=\"ml-auto mb-0 text-sm\">Forgot\n                                Password?</a>\n                        </div>\n                        <div class=\"row mb-3 px-3\"> <button type=\"submit\" class=\"btn btn-blue text-center\"\n                                (click)=\"login()\">Login</button> </div>\n                        <div class=\"row px-3\"> <small class=\"font-weight-bold\">Don't have an account? <a\n                                [routerLink]=\"['/login/signup']\" class=\"text-danger \">Register</a></small>\n                        </div>\n                    </form>\n                </div>\n            </div>\n        </div>\n    </div>\n    \n</div>"

/***/ }),

/***/ "./src/app/modules/login/login/login.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/modules/login/login/login.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 991px) {\n  .card2 {\n    margin: 0px 0px !important; }\n  .border-line {\n    border-right: none;\n    display: none; } }\n\n.card2 {\n  margin: 0px 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbG9naW4vbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSTtJQUNJLDBCQUNKLEVBQUE7RUFDQTtJQUNJLGtCQUFrQjtJQUNsQixhQUFhLEVBQUEsRUFDaEI7O0FBR0w7RUFDSSwwQkFDSixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy9sb2dpbi9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gICAgLmNhcmQyIHtcbiAgICAgICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbiAgICB9XG4gICAgLmJvcmRlci1saW5lIHtcbiAgICAgICAgYm9yZGVyLXJpZ2h0OiBub25lO1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbn1cblxuLmNhcmQyIHtcbiAgICBtYXJnaW46IDBweCAwcHggIWltcG9ydGFudFxufSJdfQ== */"

/***/ }),

/***/ "./src/app/modules/login/login/login.component.ts":
/*!********************************************************!*\
  !*** ./src/app/modules/login/login/login.component.ts ***!
  \********************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../login.service */ "./src/app/modules/login/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../config/web-urls */ "./src/app/config/web-urls.ts");







var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, router, loginService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.loginService = loginService;
        this.submitted = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.formValidation();
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    /**
     * This method is for form validation
     */
    LoginComponent.prototype.formValidation = function () {
        this.loginForm = this.formBuilder.group({
            userId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ]))
        });
    };
    /**
     * This method is to handle login
     */
    LoginComponent.prototype.login = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.submitted = true;
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(true);
                        if (!this.loginForm.valid) return [3 /*break*/, 2];
                        data = {
                            userId: this.loginForm.get('userId').value,
                            password: this.loginForm.get('password').value
                        };
                        return [4 /*yield*/, this.loginService.login$(_config_web_urls__WEBPACK_IMPORTED_MODULE_6__["LOGIN"], data).subscribe({
                                next: function (results) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    localStorage.setItem('access_token', results.data.token);
                                    localStorage.setItem('userId', results.data.userData._id);
                                    _this.router.navigate(['home/']);
                                }, error: function (error) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'error',
                                        message: error.error.message ? error.error.message : 'Not able to login at the moment. Please try again later'
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                        return [2 /*return*/];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/modules/login/login/login.component.html"),
            styles: [__webpack_require__(/*! ../../../app.component.scss */ "./src/app/app.component.scss"), __webpack_require__(/*! ../login.component.scss */ "./src/app/modules/login/login.component.scss"), __webpack_require__(/*! ./login.component.scss */ "./src/app/modules/login/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/modules/login/signup/signup.component.html":
/*!************************************************************!*\
  !*** ./src/app/modules/login/signup/signup.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto\">\n    <div class=\"card card0 border-0\">\n       <div class=\"row d-flex\">\n          \n          <div class=\"col-lg-12\">\n             <div class=\"card2 card border-0 px-4 py-5\">\n                \n                <form class=\"form-signin\" [formGroup]=\"userForm\">\n                    <div class=\"row mb-4 px-2 py-2\">\n                        <h2>Hey, please fill your details</h2>\n                     </div>\n                    <div class=\"form-group row px-3\">\n                       <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Name</h6>\n                       </label>\n                       <input formControlName=\"name\" [ngClass]=\"{ 'is-invalid': submitted && f.name.errors }\" type=\"text\"  class=\"form-control\"  placeholder=\"Enter name\"> \n                       <div *ngIf=\"submitted && f.name.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.name.errors.required\">Please add name</div>\n                       </div>\n                       \n                    </div>\n                     <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                           <h6 class=\"mb-0 text-sm\">Contact number</h6>\n                        </label>\n                        <input formControlName=\"contactNumber\" [ngClass]=\"{ 'is-invalid': submitted && f.contactNumber.errors }\" type=\"number\"  class=\"form-control\"  placeholder=\"Enter contact number\"> \n                        <div *ngIf=\"submitted && f.contactNumber.errors\" class=\"invalid-feedback\">\n                           <div *ngIf=\"f.contactNumber.errors.required\">Please add contact number</div>\n                        </div>\n                        \n                     </div>\n                     <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                           <h6 class=\"mb-0 text-sm\">Email id</h6>\n                        </label>\n                        <input formControlName=\"emailId\" [ngClass]=\"{ 'is-invalid': submitted && f.emailId.errors }\" type=\"text\"  class=\"form-control\"  placeholder=\"Enter a valid email address\"> \n                        <div *ngIf=\"submitted && f.emailId.errors\" class=\"invalid-feedback\">\n                           <div *ngIf=\"f.emailId.errors.required\">Please add email address</div>\n                           <div *ngIf=\"f.emailId.errors.pattern\">Please enter valid email id</div>\n                        </div>\n                        \n                     </div>\n                     <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                           <h6 class=\"mb-0 text-sm\">Password</h6>\n                        </label>\n                        <input formControlName=\"password\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" type=\"text\"  class=\"form-control\"  placeholder=\"Enter a password\"> \n                        <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                           <div *ngIf=\"f.password.errors.required\">Please add password</div>\n                        </div>\n                        \n                     </div>\n                    <div class=\"row mb-3 px-3\"> <button type=\"submit\" class=\"btn btn-blue text-center\" (click)=\"add()\">Add</button> </div>\n                </form>\n                \n             </div>\n          </div>\n       </div>\n    </div>\n </div>"

/***/ }),

/***/ "./src/app/modules/login/signup/signup.component.scss":
/*!************************************************************!*\
  !*** ./src/app/modules/login/signup/signup.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 991px) {\n  .card2 {\n    margin: 0px 0px !important; } }\n\n.card2 {\n  margin: 0px 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbG9naW4vc2lnbnVwL3NpZ251cC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJO0lBQ0ksMEJBQ0osRUFBQSxFQUFDOztBQUdMO0VBQ0ksMEJBQ0osRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvbG9naW4vc2lnbnVwL3NpZ251cC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gICAgLmNhcmQyIHtcbiAgICAgICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbiAgICB9XG59XG5cbi5jYXJkMiB7XG4gICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbn0iXX0= */"

/***/ }),

/***/ "./src/app/modules/login/signup/signup.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/modules/login/signup/signup.component.ts ***!
  \**********************************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../login.service */ "./src/app/modules/login/login.service.ts");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../config/web-urls */ "./src/app/config/web-urls.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var SignupComponent = /** @class */ (function () {
    function SignupComponent(formBuilder, router, loginService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.loginService = loginService;
        this.selectedWareHouse = 'Choose Warehouse';
        this.submitted = false;
    }
    SignupComponent.prototype.ngOnInit = function () {
        this.formValidation();
    };
    Object.defineProperty(SignupComponent.prototype, "f", {
        get: function () { return this.userForm.controls; },
        enumerable: true,
        configurable: true
    });
    /**
     * This method is for form validation
     */
    SignupComponent.prototype.formValidation = function () {
        this.userForm = this.formBuilder.group({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
            contactNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
            emailId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ])),
        });
    };
    /**
     * This method is to register user
     */
    SignupComponent.prototype.add = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var payload;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(true);
                        if (!this.userForm.valid) return [3 /*break*/, 2];
                        this.submitted = true;
                        payload = {
                            name: this.userForm.get('name').value,
                            password: this.userForm.get('password').value,
                            contactNumber: this.userForm.get('contactNumber').value.toString(),
                            emailId: this.userForm.get('emailId').value
                        };
                        return [4 /*yield*/, this.loginService.registerUser$(_config_web_urls__WEBPACK_IMPORTED_MODULE_5__["CREATE_USER"], payload).subscribe({
                                next: function (results) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _this.router.navigate(['login/']);
                                }, error: function (error) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'error',
                                        message: error.error.message ? error.error.message : 'Not able to register you at the moment. Please try later!'
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                        return [2 /*return*/];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/modules/login/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ../../../app.component.scss */ "./src/app/app.component.scss"), __webpack_require__(/*! ../login.component.scss */ "./src/app/modules/login/login.component.scss"), __webpack_require__(/*! ./signup.component.scss */ "./src/app/modules/login/signup/signup.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ })

}]);
//# sourceMappingURL=modules-login-login-module.js.map