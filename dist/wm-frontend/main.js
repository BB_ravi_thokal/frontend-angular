(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./modules/login/login.module": [
		"./src/app/modules/login/login.module.ts",
		"modules-login-login-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_landing_landing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/landing/landing.component */ "./src/app/modules/landing/landing.component.ts");
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/auth-guard.service */ "./src/app/services/auth-guard.service.ts");





var routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home',
    },
    {
        path: 'home',
        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
        component: _modules_landing_landing_component__WEBPACK_IMPORTED_MODULE_3__["LandingComponent"]
    },
    {
        path: 'login',
        loadChildren: './modules/login/login.module#LoginModule',
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-container *ngIf=\"loading\">\n    <div class=\"loader-background\"><div class=\"loader\"></div></div>\n</ng-container>\n\n<div style=\"margin-left: 10%; margin-right: 10%;\">\n    <div class=\"alert alert-success alert-dismissible\" *ngIf=\"isSuccess\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n        <strong>{{message}}</strong>\n    </div>\n    <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"isError\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" (click)=\"disablePopUp()\">&times;</button>\n        <strong>{{message}}</strong>\n    </div>\n\n    <div class=\"toast\" *ngIf=\"isError\">\n        <div class=\"toast-body\">\n            {{message}}\n        </div>\n    </div>\n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".text-sm {\n  font-size: 14px !important; }\n\n.toast {\n  box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(244, 67, 54, 0.4);\n  background-color: #f55145;\n  color: #ffffff;\n  display: block !important; }\n\n::-moz-placeholder {\n  color: #BDBDBD;\n  opacity: 1;\n  font-weight: 300; }\n\n::placeholder {\n  color: #BDBDBD;\n  opacity: 1;\n  font-weight: 300; }\n\n:-ms-input-placeholder {\n  color: #BDBDBD;\n  font-weight: 300; }\n\n::-ms-input-placeholder {\n  color: #BDBDBD;\n  font-weight: 300; }\n\n.input,\ntextarea {\n  padding: 10px 12px 10px 12px;\n  border: 1px solid lightgrey;\n  border-radius: 2px;\n  margin-bottom: 5px;\n  margin-top: 2px;\n  width: 100%;\n  box-sizing: border-box;\n  color: #2C3E50;\n  font-size: 14px;\n  letter-spacing: 1px; }\n\n.select {\n  padding: 5px 5px 5px 5px;\n  /*border: 1px solid lightgrey;*/\n  border-radius: 2px;\n  margin-bottom: 5px;\n  margin-top: 2px;\n  width: 100%;\n  box-sizing: border-box;\n  color: #2C3E50;\n  font-size: 14px;\n  letter-spacing: 1px; }\n\n.inputError {\n  padding: 10px 12px 10px 12px;\n  border: 1px solid red;\n  border-radius: 2px;\n  margin-bottom: 5px;\n  margin-top: 2px;\n  width: 100%;\n  box-sizing: border-box;\n  color: #2C3E50;\n  font-size: 14px;\n  letter-spacing: 1px; }\n\n.input:focus,\nselect:focus,\ntextarea:focus {\n  box-shadow: none !important;\n  border: 1px solid #304FFE;\n  outline-width: 0; }\n\n.select:focus {\n  box-shadow: none !important;\n  border: 1px solid #304FFE;\n  outline-width: 0; }\n\nbutton:focus {\n  box-shadow: none !important;\n  outline-width: 0; }\n\na {\n  color: inherit;\n  cursor: pointer; }\n\n.btn-blue {\n  background-color: #1A237E;\n  height: 50px;\n  color: #fff;\n  border-radius: 2px;\n  text-align: center; }\n\n.btn-dropDown {\n  padding: 10px 12px 10px 12px;\n  border: 1px solid lightgrey;\n  border-radius: 2px;\n  margin-bottom: 10px;\n  margin-top: 2px;\n  width: 100%;\n  box-sizing: border-box;\n  font-size: 14px;\n  letter-spacing: 1px; }\n\n.btn-dropDown:hover {\n  background-color: #000;\n  cursor: pointer; }\n\n.btn-blue:hover {\n  background-color: #000;\n  cursor: pointer; }\n\n.bg-blue {\n  color: #fff;\n  background-color: #1A237E; }\n\n.alert {\n  z-index: 9999999999 !important;\n  position: absolute !important; }\n\n.loader {\n  border-radius: 50%;\n  border-top: 4px solid;\n  border-right: 4px solid;\n  border-bottom: 4px solid;\n  border-left: 4px solid transparent;\n  width: 40px;\n  height: 40px;\n  position: fixed;\n  top: 50%;\n  margin: auto;\n  z-index: 999;\n  -webkit-animation: spin 2s linear infinite;\n  /* Safari */\n  animation: spin 2s linear infinite; }\n\n.loader-background {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(255, 255, 255, 0.9);\n  z-index: 99999999;\n  display: flex;\n  justify-content: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDBCQUNKLEVBQUE7O0FBRUE7RUFDSSxzRkFBc0Y7RUFDdEYseUJBQXlCO0VBQ3pCLGNBQWM7RUFDZCx5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSxjQUFjO0VBQ2QsVUFBVTtFQUNWLGdCQUNKLEVBQUE7O0FBSkE7RUFDSSxjQUFjO0VBQ2QsVUFBVTtFQUNWLGdCQUNKLEVBQUE7O0FBRUE7RUFDSSxjQUFjO0VBQ2QsZ0JBQ0osRUFBQTs7QUFFQTtFQUNJLGNBQWM7RUFDZCxnQkFDSixFQUFBOztBQUVBOztFQUVJLDRCQUE0QjtFQUM1QiwyQkFBMkI7RUFDM0Isa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVztFQUNYLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsZUFBZTtFQUNmLG1CQUNKLEVBQUE7O0FBRUE7RUFDSSx3QkFBd0I7RUFDeEIsK0JBQUE7RUFDQSxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsbUJBQ0osRUFBQTs7QUFFQTtFQUNJLDRCQUE0QjtFQUM1QixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVztFQUNYLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsZUFBZTtFQUNmLG1CQUNKLEVBQUE7O0FBRUE7OztFQUtJLDJCQUEyQjtFQUMzQix5QkFBeUI7RUFDekIsZ0JBQ0osRUFBQTs7QUFFQTtFQUdJLDJCQUEyQjtFQUMzQix5QkFBeUI7RUFDekIsZ0JBQ0osRUFBQTs7QUFFQTtFQUdJLDJCQUEyQjtFQUMzQixnQkFDSixFQUFBOztBQUVBO0VBQ0ksY0FBYztFQUNkLGVBQ0osRUFBQTs7QUFFQTtFQUNJLHlCQUF5QjtFQUV6QixZQUFZO0VBQ1osV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0IsRUFBQTs7QUFHdEI7RUFFSSw0QkFBNEI7RUFDNUIsMkJBQTJCO0VBQzNCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLFdBQVc7RUFDWCxzQkFBc0I7RUFFdEIsZUFBZTtFQUNmLG1CQUNKLEVBQUE7O0FBRUE7RUFDSSxzQkFBc0I7RUFDdEIsZUFDSixFQUFBOztBQUVBO0VBQ0ksc0JBQXNCO0VBQ3RCLGVBQ0osRUFBQTs7QUFFQTtFQUNJLFdBQVc7RUFDWCx5QkFDSixFQUFBOztBQUVBO0VBQ0ksOEJBQThCO0VBQzlCLDZCQUE2QixFQUFBOztBQUdqQztFQUNJLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsdUJBQXVCO0VBQ3ZCLHdCQUF3QjtFQUN4QixrQ0FBa0M7RUFDbEMsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0VBQ2YsUUFBUTtFQUNSLFlBQVk7RUFDWixZQUFZO0VBQ1osMENBQTBDO0VBQUUsV0FBQTtFQUM1QyxrQ0FBa0MsRUFBQTs7QUFFcEM7RUFDRSxlQUFlO0VBQ2YsTUFBTTtFQUNOLE9BQU87RUFDUCxXQUFXO0VBQ1gsWUFBWTtFQUNaLDBDQUEwQztFQUMxQyxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLHVCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRleHQtc20ge1xuICAgIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50XG59XG5cbi50b2FzdCB7XG4gICAgYm94LXNoYWRvdzogMCA0cHggMjBweCAwcHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCA3cHggMTBweCAtNXB4IHJnYmEoMjQ0LCA2NywgNTQsIDAuNCk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y1NTE0NTtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xufVxuXG46OnBsYWNlaG9sZGVyIHtcbiAgICBjb2xvcjogI0JEQkRCRDtcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtd2VpZ2h0OiAzMDBcbn1cblxuOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgY29sb3I6ICNCREJEQkQ7XG4gICAgZm9udC13ZWlnaHQ6IDMwMFxufVxuXG46Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgY29sb3I6ICNCREJEQkQ7XG4gICAgZm9udC13ZWlnaHQ6IDMwMFxufVxuXG4uaW5wdXQsXG50ZXh0YXJlYSB7XG4gICAgcGFkZGluZzogMTBweCAxMnB4IDEwcHggMTJweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyZXk7XG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICBtYXJnaW4tdG9wOiAycHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBjb2xvcjogIzJDM0U1MDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweFxufVxuXG4uc2VsZWN0IHtcbiAgICBwYWRkaW5nOiA1cHggNXB4IDVweCA1cHg7XG4gICAgLypib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyZXk7Ki9cbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGNvbG9yOiAjMkMzRTUwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4XG59XG5cbi5pbnB1dEVycm9yIHtcbiAgICBwYWRkaW5nOiAxMHB4IDEycHggMTBweCAxMnB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJlZDtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGNvbG9yOiAjMkMzRTUwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4XG59XG5cbi5pbnB1dDpmb2N1cyxcbnNlbGVjdDpmb2N1cyxcbnRleHRhcmVhOmZvY3VzIHtcbiAgICAtbW96LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzMwNEZGRTtcbiAgICBvdXRsaW5lLXdpZHRoOiAwXG59XG5cbi5zZWxlY3Q6Zm9jdXMge1xuICAgIC1tb3otYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMzA0RkZFO1xuICAgIG91dGxpbmUtd2lkdGg6IDBcbn1cblxuYnV0dG9uOmZvY3VzIHtcbiAgICAtbW96LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgb3V0bGluZS13aWR0aDogMFxufVxuXG5hIHtcbiAgICBjb2xvcjogaW5oZXJpdDtcbiAgICBjdXJzb3I6IHBvaW50ZXJcbn1cblxuLmJ0bi1ibHVlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMUEyMzdFO1xuICAgIC8vd2lkdGg6IDE1MHB4O1xuICAgIGhlaWdodDogNTBweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYnRuLWRyb3BEb3duIHtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMUEyMzdFO1xuICAgIHBhZGRpbmc6IDEwcHggMTJweCAxMHB4IDEycHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmV5O1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIC8vIGNvbG9yOiAjMkMzRTUwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4XG59XG5cbi5idG4tZHJvcERvd246aG92ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG4gICAgY3Vyc29yOiBwb2ludGVyXG59XG5cbi5idG4tYmx1ZTpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcbiAgICBjdXJzb3I6IHBvaW50ZXJcbn1cblxuLmJnLWJsdWUge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMxQTIzN0Vcbn1cblxuLmFsZXJ0IHtcbiAgICB6LWluZGV4OiA5OTk5OTk5OTk5ICFpbXBvcnRhbnQ7XG4gICAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG59XG5cbi5sb2FkZXIge1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBib3JkZXItdG9wOiA0cHggc29saWQ7XG4gICAgYm9yZGVyLXJpZ2h0OiA0cHggc29saWQ7XG4gICAgYm9yZGVyLWJvdHRvbTogNHB4IHNvbGlkO1xuICAgIGJvcmRlci1sZWZ0OiA0cHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgd2lkdGg6IDQwcHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB0b3A6IDUwJTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgei1pbmRleDogOTk5O1xuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTsgLyogU2FmYXJpICovXG4gICAgYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTtcbiAgfVxuICAubG9hZGVyLWJhY2tncm91bmQge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjkpO1xuICAgIHotaW5kZXg6IDk5OTk5OTk5O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: LoaderState$, PopUpState$, AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderState$", function() { return LoaderState$; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopUpState$", function() { return PopUpState$; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var LoaderState$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
var PopUpState$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.isError = false;
        this.isSuccess = false;
        this.loading = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                PopUpState$
                    .subscribe(function (data) {
                    if (data.state.toLowerCase() === 'success') {
                        _this.isSuccess = true;
                        _this.isError = false;
                        _this.message = data.message;
                    }
                    else if (data.state.toLowerCase() === 'error') {
                        _this.isError = true;
                        _this.isSuccess = false;
                        _this.message = data.message;
                    }
                    else {
                        _this.isError = false;
                        _this.isSuccess = false;
                        _this.message = '';
                    }
                });
                LoaderState$
                    .subscribe(function (data) {
                    if (data) {
                        _this.loading = true;
                    }
                    else {
                        _this.loading = false;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    AppComponent.prototype.disablePopUp = function () {
        this.isError = false;
        this.isSuccess = false;
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _modules_landing_landing_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/landing/landing.component */ "./src/app/modules/landing/landing.component.ts");
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/auth-guard.service */ "./src/app/services/auth-guard.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _modules_landing_landing_component__WEBPACK_IMPORTED_MODULE_6__["LandingComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"]
            ],
            providers: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["CUSTOM_ELEMENTS_SCHEMA"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/config/web-urls.ts":
/*!************************************!*\
  !*** ./src/app/config/web-urls.ts ***!
  \************************************/
/*! exports provided: BASE_URL, LOGIN, FORGOT_PASSWORD, CHANGE_PASSWORD, CREATE_USER, GET_USER, UPDATE_USER */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BASE_URL", function() { return BASE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LOGIN", function() { return LOGIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FORGOT_PASSWORD", function() { return FORGOT_PASSWORD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHANGE_PASSWORD", function() { return CHANGE_PASSWORD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATE_USER", function() { return CREATE_USER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_USER", function() { return GET_USER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_USER", function() { return UPDATE_USER; });
var BASE_URL = 'http://localhost:4000/';
// login urls
var LOGIN = 'login';
var FORGOT_PASSWORD = 'login/forgot-password';
var CHANGE_PASSWORD = 'login/change-password';
// Warehouse urls
var CREATE_USER = 'user/create';
var GET_USER = 'user';
var UPDATE_USER = 'user/update';


/***/ }),

/***/ "./src/app/modules/landing/landing.component.html":
/*!********************************************************!*\
  !*** ./src/app/modules/landing/landing.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto\">\n  <div style=\"text-align: right\">\n      <button class=\"btn btn-blue\" (click)=\"logout()\">Logout</button>\n  </div>\n  \n  <div class=\"card card0 border-0\">\n     <div class=\"row d-flex\">\n        <div class=\"col-lg-12\">\n           <div class=\"card2 card border-0 px-4 py-5\">\n              \n              <form class=\"form-signin\" [formGroup]=\"userForm\">\n                  <div class=\"form-group row px-3\">\n                     <label class=\"mb-1\">\n                        <h6 class=\"mb-0 text-sm\">Name</h6>\n                     </label>\n                     <input formControlName=\"name\" [ngClass]=\"{ 'is-invalid': submitted && f.name.errors }\" type=\"text\" \n                      class=\"form-control\"  placeholder=\"Enter name\"> \n                     <div *ngIf=\"submitted && f.name.errors\" class=\"invalid-feedback\">\n                        <div *ngIf=\"f.name.errors.required\">Please add name</div>\n                     </div>\n                     \n                  </div>\n                   <div class=\"form-group row px-3\">\n                      <label class=\"mb-1\">\n                         <h6 class=\"mb-0 text-sm\">Contact number</h6>\n                      </label>\n                      <input formControlName=\"contactNumber\" [ngClass]=\"{ 'is-invalid': submitted && f.contactNumber.errors }\" type=\"number\"\n                        class=\"form-control\"  placeholder=\"Enter contact number\"> \n                      <div *ngIf=\"submitted && f.contactNumber.errors\" class=\"invalid-feedback\">\n                         <div *ngIf=\"f.contactNumber.errors.required\">Please add contact number</div>\n                      </div>\n                      \n                   </div>\n                   <div class=\"form-group row px-3\">\n                      <label class=\"mb-1\">\n                         <h6 class=\"mb-0 text-sm\">Email id</h6>\n                      </label>\n                      <input formControlName=\"emailId\" [ngClass]=\"{ 'is-invalid': submitted && f.emailId.errors }\" type=\"text\"\n                        class=\"form-control\"  placeholder=\"Enter a valid email address\"> \n                      <div *ngIf=\"submitted && f.emailId.errors\" class=\"invalid-feedback\">\n                         <div *ngIf=\"f.emailId.errors.required\">Please add email address</div>\n                         <div *ngIf=\"f.emailId.errors.pattern\">Please enter valid email id</div>\n                      </div>\n                      \n                   </div>\n                  <div class=\"row mb-3 px-3\"> <button type=\"submit\" class=\"btn btn-blue text-center\" (click)=\"update()\">Update</button> </div>\n              </form>\n              \n           </div>\n        </div>\n     </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/modules/landing/landing.component.scss":
/*!********************************************************!*\
  !*** ./src/app/modules/landing/landing.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 991px) {\n  .card2 {\n    margin: 0px 0px !important; } }\n\n.card2 {\n  margin: 0px 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbGFuZGluZy9sYW5kaW5nLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0k7SUFDSSwwQkFDSixFQUFBLEVBQUM7O0FBR0w7RUFDSSwwQkFDSixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy9sYW5kaW5nL2xhbmRpbmcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAgIC5jYXJkMiB7XG4gICAgICAgIG1hcmdpbjogMHB4IDBweCAhaW1wb3J0YW50XG4gICAgfVxufVxuXG4uY2FyZDIge1xuICAgIG1hcmdpbjogMHB4IDBweCAhaW1wb3J0YW50XG59Il19 */"

/***/ }),

/***/ "./src/app/modules/landing/landing.component.ts":
/*!******************************************************!*\
  !*** ./src/app/modules/landing/landing.component.ts ***!
  \******************************************************/
/*! exports provided: LandingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingComponent", function() { return LandingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _landing_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./landing.service */ "./src/app/modules/landing/landing.service.ts");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../config/web-urls */ "./src/app/config/web-urls.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var LandingComponent = /** @class */ (function () {
    function LandingComponent(formBuilder, router, landingService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.landingService = landingService;
        this.selectedWareHouse = 'Choose Warehouse';
        this.submitted = false;
    }
    LandingComponent.prototype.ngOnInit = function () {
        this.getUserData();
        this.formValidation();
    };
    Object.defineProperty(LandingComponent.prototype, "f", {
        get: function () { return this.userForm.controls; },
        enumerable: true,
        configurable: true
    });
    /**
     * This method is to fetch latest user details and display
     */
    LandingComponent.prototype.getUserData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, localStorage.getItem('userId')];
                    case 1:
                        _a.userId = _b.sent();
                        return [4 /*yield*/, this.landingService.getUser$(_config_web_urls__WEBPACK_IMPORTED_MODULE_5__["GET_USER"], { userId: this.userId }).subscribe({
                                next: function (results) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _this.userData = results.data.data;
                                    _this.userForm.controls.name.setValue(_this.userData.name);
                                    _this.userForm.controls.contactNumber.setValue(_this.userData.contactNumber);
                                    _this.userForm.controls.emailId.setValue(_this.userData.emailId);
                                }, error: function (error) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'error',
                                        message: error.error.message ? error.error.message : 'Not able to fetch user details. Please try later!'
                                    });
                                }
                            })];
                    case 2:
                        _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * This method is for form validation
     */
    LandingComponent.prototype.formValidation = function () {
        this.userForm = this.formBuilder.group({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
            contactNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
            emailId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ])),
        });
    };
    /**
     * This method is to update user details
     */
    LandingComponent.prototype.update = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var payload;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(true);
                        this.submitted = true;
                        if (!this.userForm.valid) return [3 /*break*/, 2];
                        payload = {
                            _id: this.userId,
                            name: this.userForm.get('name').value,
                            contactNumber: this.userForm.get('contactNumber').value,
                            emailId: this.userForm.get('emailId').value
                        };
                        return [4 /*yield*/, this.landingService.updateUser$(_config_web_urls__WEBPACK_IMPORTED_MODULE_5__["UPDATE_USER"], payload).subscribe({
                                next: function (results) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'success',
                                        message: results.message ? results.message : 'Update success!'
                                    });
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _this.getUserData();
                                }, error: function (error) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'error',
                                        message: error.error.message ? error.error.message : 'Not able to fetch user details. Please try later!'
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                        return [3 /*break*/, 3];
                    case 2:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                        return [2 /*return*/];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LandingComponent.prototype.logout = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, localStorage.clear()];
                    case 1:
                        _a.sent();
                        this.router.navigate(['login/']);
                        return [2 /*return*/];
                }
            });
        });
    };
    LandingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-landing',
            template: __webpack_require__(/*! ./landing.component.html */ "./src/app/modules/landing/landing.component.html"),
            styles: [__webpack_require__(/*! ./landing.component.scss */ "./src/app/modules/landing/landing.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _landing_service__WEBPACK_IMPORTED_MODULE_4__["LandingService"]])
    ], LandingComponent);
    return LandingComponent;
}());



/***/ }),

/***/ "./src/app/modules/landing/landing.service.ts":
/*!****************************************************!*\
  !*** ./src/app/modules/landing/landing.service.ts ***!
  \****************************************************/
/*! exports provided: LandingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingService", function() { return LandingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../config/web-urls */ "./src/app/config/web-urls.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var LandingService = /** @class */ (function () {
    function LandingService(http) {
        this.http = http;
    }
    /**
     * This method is for Get Api calls
     * @param url String Api URL
     * @param params Object required for the Get request
     * @returns Return the respective response from the Apis.
     */
    LandingService.prototype.getUser$ = function (url, params) {
        var accessToken = localStorage.getItem('access_token');
        var httpOptions;
        var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
            .set('userId', params.userId);
        if (accessToken) {
            httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get(_config_web_urls__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"] + url, httpOptions);
    };
    /**
     * This method is for Put Api calls
     * @param url String Api URL
     * @param requestBody Object required for the Post request
     * @returns Return the respective response from the Apis.
     */
    LandingService.prototype.updateUser$ = function (url, requestBody) {
        var accessToken = localStorage.getItem('access_token');
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                // tslint:disable-next-line:object-literal-key-quotes
                authorization: accessToken,
            })
        };
        return this.http.put(_config_web_urls__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"] + url, requestBody, httpOptions);
    };
    LandingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], LandingService);
    return LandingService;
}());



/***/ }),

/***/ "./src/app/services/auth-guard.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/auth-guard.service.ts ***!
  \************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    /**
     * @description This method will check user already login or not
     * @returns Return the true if user already logged in or else false
     */
    AuthGuard.prototype.canActivate = function () {
        try {
            var isTokenAvailable = localStorage.getItem('access_token');
            if (isTokenAvailable) {
                return true;
            }
            else {
                this.router.navigate(['/login']);
                return false;
            }
        }
        catch (err) {
            return false;
        }
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/macbook/Desktop/Ravi/WM/wm-frontend/wm-frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map